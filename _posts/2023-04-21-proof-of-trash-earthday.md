---
title: 'Proof of Trash: A Verifiable Earth Day Beach Trash Clean-up'
date: 2023-04-21 10:00:00
featured_image: '/images/earthday2023.jpg'
excerpt: 'A pessimists guide to verifiable, cryptographically-notarized beach clean-ups'
---


As Earth Day approaches every year, we all get that familiar pang of guilt. We know we should do something to help the planet, so we eagerly make plans to participate in a beach cleanup. Armed with good intentions and reusable bags, we head to the shore ready to make a difference. But let's be real - our lazy and pessimistic selves often take over, and the results are less than impressive.

[![a collage of photos of trash collected on the beach](/images/earthday2023.jpg)](https://cloudflare-ipfs.com/ipfs/bafybeigg5j7r2mamfdshwycxgp243tndllhg3vlj6l4y6z5u2h2nkq4s2m/Earthday-proofmode-4FE853885FA02C4C-2023-04-21-08-25-32EDT.zip)

The original full-resolution, verified, signed, and notarized photos above are available in a Proof bundle is stored on Web3.storage at [this link](https://cloudflare-ipfs.com/ipfs/bafybeigg5j7r2mamfdshwycxgp243tndllhg3vlj6l4y6z5u2h2nkq4s2m/Earthday-proofmode-4FE853885FA02C4C-2023-04-21-08-25-32EDT.zip). Just to make you feel more guilty about not doing enough for our planet, all of this trash was collected by my 9 and 12 year old kids early on a Thursday morning.

### It all starts with the early morning alarm clock that rudely interrupts our precious weekend sleep-in. We drag ourselves out of bed, grumbling about why Earth Day couldn't fall on a weekday when we're already up and about. We muster up some enthusiasm, put on our "Save the Earth" t-shirts, and head to the beach, hoping to make a meaningful impact.

As we arrive at the beach, we quickly realize that the trash situation is worse than we imagined. Plastic bottles, cigarette butts, and snack wrappers are scattered everywhere. We exchange nervous glances with our fellow beach clean-up volunteers, all silently questioning if we've bitten off more than we can chew.

![map of where we found trash](/images/truromap1.png)

![another map of trash](/images/truromap2.png)

*View an [interactive geojson map of all the trash data points](https://geojson.io/#data=data:text/x-url,https://bafybeibitr3buzvncogknrusx3gctefrw7p7f5n6wj6fcocxbz2rz4dyeq.ipfs.w3s.link/ipfs/bafybeibitr3buzvncogknrusx3gctefrw7p7f5n6wj6fcocxbz2rz4dyeq/earthdaytruro2023a.json) from our 2023 earth day beach clean-up in Truro, Cape Code, Massachusetts* 

Armed with gloves and trash bags, we start picking up the trash. But soon enough, we realize that it's not as easy as it looks. We have to bend down repeatedly, squat, and even dig through the sand to reach the tiny pieces of plastic that seem to multiply like rabbits. Our lazy muscles start to ache, and we wonder if there's a beach cleanup workout routine we should've followed to prepare for this.

As we continue our cleanup, we encounter the dreaded pessimism. We find ourselves questioning if our efforts are even making a difference. Will picking up a few bags of trash from the beach really save the planet? The pessimistic voice in our heads says, "It's just a drop in the ocean - pun intended - compared to the massive environmental challenges we face."

And then there are the inevitable distractions. The beach is a beautiful place, after all, and our lazy minds start wandering. We spot a group of surfers riding the waves, and suddenly, we're itching to catch some waves ourselves.

### Or maybe we see a cute dog running along the shore, and we can't resist the urge to play fetch. Before we know it, we've abandoned our trash bags and joined the distractions, convincing ourselves that the beach is just too enjoyable to pass up.

![a cute dog at earthday](/images/earthdaydog.jpg)

*This is an actual cute dog that exists, and participated in the beach clean-up*

After a few hours of half-hearted cleanup, we gather for a group photo, holding up our meager collection of trash bags as proof of our efforts. We post the photo on social media with hashtags like #SaveTheEarth and #BeachCleanup, hoping to inspire others. But deep down, we know that we could've done more, and our lazy and pessimistic selves start to justify our lackluster efforts.

![Verified Beach Clean-up](/images/verifiedearthday.png)

*You can verify our efforts to provide a clean beach and ocean for the sea turtles, by [downloading the proof bundle zip](https://cloudflare-ipfs.com/ipfs/bafybeigg5j7r2mamfdshwycxgp243tndllhg3vlj6l4y6z5u2h2nkq4s2m/Earthday-proofmode-4FE853885FA02C4C-2023-04-21-08-25-32EDT.zip), and using our [ProofCheck Verification Tool](https://proofcheck.gpfs.link)*

As we pack up and head home, we promise ourselves that we'll be more proactive next year. We'll start preparing for the cleanup weeks in advance, we'll recruit more volunteers, and we'll bring snacks and music to keep ourselves motivated. But let's be honest, we know that when Earth Day rolls around again, our lazy and pessimistic selves will probably resurface, and we'll find ourselves in a familiar cycle of good intentions, distractions, and excuses.

So here's to all the lazy and pessimistic Earth Day beach cleanup volunteers out there. We may not save the planet in a day, but at least we can say we tried, right? And hey, maybe one day we'll surprise ourselves and actually make a meaningful impact. Until then, we'll keep showing up on Earth Day with our reusable bags and half-hearted efforts, because it's the thought that counts, right? 

### Or, you can commit to using [ProofMode on your phone](/install) to turn your photos and videos into actionable, meaningful evidence and data. Learn more about how you can join our [ProofCorps Team](/proofcorps) to document the planet using verifiable smartphone photos and video, to do something meaningful this Earth Day, and every day!

<hr/>
# Happy Earth Day, everyone! 
