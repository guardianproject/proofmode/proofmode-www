---
title: 'BASELINE: ProofMode for Indigenous Rights in Mexico'
date: 2024-02-09 10:00:00
featured_image: '/images/baseline/caravan2023.png'
excerpt: We invited team members of the "El Sur Resiste" movement to the Guardian Project podcast to share with us their experiences using ProofMode to document the negative impact of the Mayan Train development in southern states of Mexico.
author: Fabiola
tag: baseline
categories: [baseline, mexico]
tags: baseline
---

We invited team members of the [El Sur Resiste](https://www.elsurresiste.org/) movement to the [Guardian Project podcast](https://guardianproject.info/podcast/) to share with us their experiences using [ProofMode](https://proofmode.org) to document the negative impact of the Mayan Train development in southern states of Mexico.

_Invitamos a miembros del equipo del movimiento El Sur Resiste al podcast del proyecto Guardian para compartir con nosotros sus experiencias utilizando ProofMode para documentar el impacto negativo del desarrollo del Tren Maya en los estados del sur de México._

[![Baseline Caravan Image](/images/baseline/caravan2023.png)](/baseline/#caravan2023)

___ProofMode verifiable images from the [Indigenous Caravan ProofSet](/baseline/#caravan2023)___

We were joined by Nicolas and Armando from the [Laboratorio de Medios Libres](https://laboratoriodemedios.org/) who were in charge of the communications logistics of the caravan. Using [ProofMode](https://proofmode.org), they documented their travels and the many environmental and social issues being caused by the Mayan Train development project. They contributed their verifiable photos and videos to [ProofMode Baseline](/baseline) for preservation and storage on the [IPFS](https://ipfs.io) and [Filecoin](https://filecoin.io) decentralized and immutable storage networks.

_Nos acompañaron Nicolás y Armando del Laboratorio de Medios Libres, quienes estaban a cargo de la logística de comunicaciones de la caravana. Utilizando ProofMode, documentaron sus viajes y los numerosos problemas ambientales y sociales causados por el proyecto de desarrollo del Tren Maya. Contribuyeron con sus fotos y videos verificables a ProofMode Baseline para su preservación y almacenamiento en las redes de almacenamiento descentralizadas e inmutables IPFS y Filecoin._

### Original interview in Spanish with Captions
<iframe width="560" height="315" src="https://www.youtube.com/embed/ENYI-GLYbEQ?si=tgW0hTGR3mUUYSMb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### English Dubbed Version (ElevenLab)
<iframe width="560" height="315" src="https://www.youtube.com/embed/zr8MC6Gbw0g?si=Gf6YtK0xYZxfuX7B" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

