---
title: 'Data Week - Colombia Edition'
date: 2023-03-30 00:00:00
featured_image: /images/data-week-0.jpg
excerpt: A celebration of data sovereignty and integrity in Latin America
---

[Data Week](https://docutopia.sustrato.red/dataweek:16#Preparativos) is a week-long celebration of [Open Data Day](https://opendataday.org), an international, annual festivity of open data. It is sponsored by the [Open Knowledge Foundation](https://okfn.org), a global, non-profit organization with the goal of making data accessible and understandable for everyone.

![](/images/data-week-1.png)

I celebrated Data Week in Bogotá, Colombia, at the local hackerspace, called [Hackbo](https://hackbo.org). Hackbo is a collective rooted in collectivism and activism–or hacktivism–composed of a diverse group of professors, musicians, animators, librarians, and of course, hackers. Data Week is run by the members of [Grafoscopio](https://mutabit.com/grafoscopio/index.en.html), a versatile tool for data visualization and documentation, with a focus on citizen science. Sessions were led by [Dr. Offray Luna](https://medium.com/lucid-archive/hackers-in-bogotá-80e70f6dbb96), academic/activist at [Pontificia Universidad Javeriana](https://en.wikipedia.org/wiki/Pontificia_Universidad_Javeriana) and founder of Grafoscopio.

![](/images/data-week-2.jpg)

Some of the topics covered by this included [Decolonial Theory as Sociotechnical Foresight in Artificial Intelligence](https://arxiv.org/pdf/2007.04068.pdf) and [Data Feminism](https://data-feminism.mitpress.mit.edu). We discussed the new exploitation of Latin American resources in the form of information gathering as fodder for training machine learning algorithms deployed by the United States and Europe, as described in 
Access Now’s [Made Abroad, Deployed at Home](https://www.accessnow.org/wp-content/uploads/2021/08/Surveillance-Tech-Latam-Report.pdf). An intersectional, interdisciplinary approach to understanding ethical data acquisition was fundamental to our analysis. Music was also a key part of the experience, with pauses for dancing and [a mesmerizing display of livecoding](https://www.youtube.com/watch?v=xXNB1BbKY8A).

On our final day of Data Week, we dove into our [ProofMode workshop](https://mutabit.com/repos.fossil/mutabit/file?name=wiki/es/DataWeek/16-6.md&ci=tip). [ProofMode’s ProofCheck](https://proofcheck-dev.gpfs.link) tool provides data sovereignty to the user. Users can drag and drop ProofMode zip files into the ProofCheck tool, which empowers people to verify their proof and analyze their evidence.

![](/images/data-week-3.png)

ProofCheck checks the media signatures, presence of proof data, [OpenTimeStamps](https://opentimestamps.org) notarization, [Google SafetyNet](https://developer.android.com/training/safetynet/attestation) attestation, and will soon check [C2PA](https://c2pa.org) verification as well! One can easily download a KML file to import into Google Earth, or a GeoJSON file for analysis.

![](/images/data-week-4.png)

With the KML file imported into Google Earth, you can cross-reference your proof with Google Earth images in a verifiable and encrypted way.

![](/images/data-week-5.png)

![](/images/data-week-6.png)

With future versions of ProofCheck, we will provide a concatenated JSON and CSV file of all proof data uploaded. This is to allow users autonomy over how they analyze data, using tools such as Tableau and [Glamorous Toolkit](https://gtoolkit.com).

Data Week was an enriching experience, crossing the borders of philosophy and hackathons, opening up discussions for ephemeral ideas and real world applications. We look forward to participating in Open Data Day again.
