---
title: 'ProofMode in the Newsroom'
date: 2024-03-04 10:00:00
featured_image: '/images/posts/proofmodenewsfabby.png'
excerpt: Fabiola walks through how how ProofMode can be used by journalists and newsroom to capture and verify photos and videos.
author: Fabiola
categories: [newsroom]
tags: newsroom, journalism
tag: newsroom, journalism
---

Learn from Fabiola Maurice, Community Lead, on how you can use the free and open-source ProofMode apps for Android and iOS, along with the [ProofCheck web tool](https://proofcheck.gpfs.link/) the emerging standards from the [Coalition for Content Provenance and Authentication (C2PA)](https://c2pa.org) and the [Content Authenticity Initiative](https://contentauthenticity.org/)

The full slides from this presentation [are available here](https://www.beautiful.ai/player/-NfDEunb5lNGfa3hoiTC). We can also provide online and in-person trainings for news organization, journalists, election monitoring groups, and others. [Contact us here](/contact)

<iframe width="560" height="315" src="https://www.youtube.com/embed/SXaP_8veUyk?si=E_DjvKy32dSe3lFQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

More presentations, videos, and documentation available [on our Links page](/links)
