---
title: 'Data Feminism: Power and Proof'
date: 2023-07-10 00:00:00
featured_image: /images/data_feminism_0.jpeg
excerpt: An Intersectional Approach to Gathering, Analyzing, and Sharing Data
---
*By Jack Fox Keen*

What *is* Data Feminism?

Data Feminism is the process of applying feminism—specifically the [Black radical feminist tradition](https://en.wikipedia.org/wiki/Black_feminism#:~:text=Black%20feminism%20philosophy%20centers%20on,as%20human%20persons%20for%20autonomy.%22) of intersectionality—to the process of data science. Black feminism challenges white feminism by emphasizing the different factors that oppress communities and individuals, in addition to gender. Angela Davis’ [Women, Race, and Class](https://en.wikipedia.org/wiki/Women,_Race_and_Class) is a prime example of the study of intersectionality as a Black feminist principle. This blog post, and the [corresponding presentation](https://docs.google.com/presentation/d/1k0XFnJ4NYUeoL9kPz5DRKi6OMMSnPCaEV1E-rQb0D1s/edit?usp=sharing) at [DWeb Camp](https://dwebcamp.org), is directly inspired by the book titled *[Data Feminism](https://mitpress.mit.edu/9780262044004/)*, by Catherine D’Ignazio and Lauren F. Klein. (The book is available for free at MIT Press! Each chapter can be downloaded as a PDF.) Data Feminism includes the following principles:

1. Examine Power
2. Challenge Power
3. Elevate Emotion and Embodiment
4. Rethink Binaries and Hierarchies
5. Embrace Pluralism
6. Consider Context
7. Make Labor Visible

**The first principle is to examine power**—to look at the power structures that exist in our current world, such as systemic racism, white supremacy, patriarchy, imperialism, and colonialism. We must also look at the [matrix of domination](https://en.wikipedia.org/wiki/Matrix_of_domination) that exists to either restrict our movement through society (oppression) or enable our movement through society (privilege). Data Feminism asks that we look at power structures in our society and [how they are reflected in ourselves](https://youtu.be/PU5zl1raW4Y?t=26). For example, within myself I exhibit the following power dynamics:

*Factors in my life that have restricted my movement in society, due to racism, sexism, cis-heteronormativity, and more:*
* Latinx/Mixed race
* Single-parent household
* Neurodivergent (as opposed to neurotypical)
* Nonbinary (but perceived as a woman, with all the discrimination that entails)
* Queer

*Factors in my life that have enabled my movement through society, due to white supremacy, classism, and ableism:*
* White passing
* Upper-middle-class upbringing
* Able-bodied
* Higher education degree

We can expand this to think about power structures in our technology as well. For example, there is Google Drive, Amazon cloud services, Adobe cloud services, among others. All of these services exist within a centralized “technocracy,” which creates a very vulnerable position for activists and human rights campaigns. Within such systems is the routine extraction of data for less-than-altruistic, and oftentimes nefarious, purposes. Storing photos, videos, and other media files on such systems can result in exploitation, surveillance, and even censorship.

**The second principle is to challenge power.** When we challenge power, we are seeking to dismantle the systems that prop up power structures as opposed to focusing on individual people or algorithms. Instead of thinking about why one company’s facial recognition software failed to recognize women of color, we think about the larger implications of the prison industrial complex necessitating those systems in the first place, and the risk this poses for communities of color and other marginalized groups.

Regarding technocracy, decentralized systems like the [Interplanetary File System (IPFS)](https://ipfs.tech) challenge Google Drive with respect to media storage. Instead of one server or hub of centralized servers as sole actors for archiving important documents, IPFS spreads the storage out over voluntary users using various IPFS nodes. If one server goes down, there is another to take its place. Now, accessing photos is no longer vulnerable to [Instagram outages](https://www.outlookindia.com/international/instagram-suffers-global-outage-as-app-crashes-and-glitches-frustrate-users-news-301122), or [social media censorship](https://www.hrw.org/news/2021/10/08/israel/palestine-facebook-censors-discussion-rights-issues).

At ProofMode, our [ProofCorps](https://proofmode.org/proofcorps) team is challenging Google Earth with our ProofMode Baseline Challenge—we are seeking to verifiably document the world by generating public “Proof Packs” of photos, videos, and associated metadata—free from filters, alterations, or artificial intelligence augmentation. We have embarked on this journey around the world, with journalists in the Middle East, artists in South Africa and Europe, and Indigenous Earth defenders in Latin America. Of particular note is our Indigenous Caravan team, which has been documenting the devastating construction of the [Mayan Train](https://www.theguardian.com/global-development/2023/may/23/fury-as-maya-train-nears-completion-mexico), described as a “Mega Project of Death.” This train connects tourist locations in Mexico at the expense of local and Indigenous communities, as well as protected environmental sites. Almost 3,000 families have been displaced for the creation of this train, and endangered species such as the jaguar are losing their habitats.

[Video 1 - Train Tracks](https://drive.google.com/file/d/19TuEEorxKIVwX4BbzB_NouQneq_Lvp_w/view?usp=sharing)

The military has become heavily involved in this construction, a clear example of imperialism and colonialism over Indigenous citizens of Mexico. ProofCorps teamed up with various community members to document the disastrous effects of the project, as well as highlight resistance by those directly impacted. They are courageously challenging the structures of imperialism and colonialism of the Mexican government and the military industrial complex.

[Video 2 - Military Involvement](https://drive.google.com/file/d/1gZQFTy8yiCSNRwJqh1HOS_3BMB2rKBI_/view?usp=sharing)

**The third principle of Data Feminism is to elevate emotion and embodiment.** To do this, we actually have to break down a false binary between emotion and logic, which is rooted in misogynistic thinking of the “feminine” and “masculine” knowledge systems. Because women are stereotypically associated with emotion, and conversely men are associated with logic, our society upholds logic as the only true, reliable source of information. This is a false dichotomy, as scientists are now realizing that emotions actually help people understand and retain information better. The two should not be separated, and arguably cannot be separated. In fact, emotion must be elevated.

In the following example, we can see a still of [an animated graphic](https://guns.periscopic.com) of gun deaths in the United States. This animation was stirring, moving, and invoked an instinctual response of sadness and mourning for the lives lost. It was praised for its beauty, simplicity, and reality of the situation. The lives lost tell a story for the individuals who comprise the dataset.

![](/images/data_feminism_1.jpg)

![](/images/data_feminism_2.jpg)

However, this data storytelling approach was lambasted by prominent data scientists as being “too emotional.” It was argued that a more “neutral” graphic should be used to tell this story, as shown in the following graphic from the [Washington Post](https://www.washingtonpost.com/news/wonk/wp/2016/06/16/fbi-active-shooter-incidents-have-soared-since-2000/). Here, we can see that the contextual knowledge of the individual life is lost, and we don’t even know how many deaths are associated with each number of active shooter incidents. It is neutral, and it is lacking.

![](/images/data_feminism_3.jpg)

In the following video, we can see the effects of climate change, a deleterious process to which the Mayan Train will directly contribute. As the video progresses, you can see the route along the coastline is coming under water as the tide splashes onto the road. One can easily foresee the future, where this entire path will be inundated under water. This invokes fear in the viewer—Data Feminism argues that this data point is stronger, not weaker, because of the emotional context.

[Video 3 - Water on Roadway and the Effects of Global Warming](https://drive.google.com/file/d/1udnyjxSvp2SDVq2_SA-1ZyLaFD76wlgT/view?usp=sharing)

As hinted in the previous principle, **the fourth principle is to rethink binaries and hierarchies** starting with biological sex. Biologists are now realizing that [the male-female sex binary is overly simplistic](https://www.scientificamerican.com/article/sex-redefined-the-idea-of-2-sexes-is-overly-simplistic1/) and that there are mosaic genetics and intersex organisms that lie on a spectrum between these two sexes. At ProofMode, we had to rethink our notions of “True” versus “False” when it comes to metadata that is captured. Does the lack of location data mean an image is “false”? Not necessarily. Things can exist on a spectrum of verifiability, and [we provide the data](https://proofmode.org/blog/data-legend-blogpost) in a digestible way for people to draw their own conclusions about which proof to utilize for their specific circumstances.

**The fifth principle is to embrace pluralism.** We need to focus on a wide variety of perspectives from different people, particularly focusing on the margins of society, which are so often overlooked by those at the center. I am often reminded of a story during the start of the pandemic, told to me by artist and activist [Nik Rye](https://www.nikrye.com). Water fountains were being shut down in public spaces and restaurants were closing. Nik realized that these minor inconveniences to the majority of people during lockdown were catastrophic life-or-death situations for a community often overlooked and unheard: unhoused people. Nik had this insight because they themselves had been unhoused and knew that while unhoused, they needed to use public restrooms and water fountains to survive. This is powerful because it reminds us that we have so many unconscious biases and ignorance that we’re not even aware of until someone with different lived experiences points them out to us.

Proof Baseline embraces pluralism by capturing a variety of perspectives from different people as they document the world around them. These different angles and viewpoints can be compared with the current documentation in Google Drive, as our [ProofCheck tool](https://proofcheck.gpfs.link) generates KML and GeoJSON files which can be uploaded into Google Earth for a side-by-side analysis. (For more on ProofCheck, check out our blogpost about [Data Week in Colombia](https://proofmode.org/blog/data-week-colombia-edition!)

Pluralism is also captured by our Indigenous Caravan, as we can see from the numerous groups and organizations coming together to protest against the Mayan Train.

[Video 4 - Protest with Lots of Banners](https://drive.google.com/file/d/1rW12NFdtmV1hAh3oB4vfkgZrvAxaeIDu/view?usp=sharing)

Data Feminism asserts that **we must also consider context, our sixth principle**, which means understanding that data are not neutral or objective, nor can they be siloed from the situations that give rise to the data. The following graphs from *Data Feminism* show the same bar chart with some slight changes, sometimes subtle, in the title and subtitle.

![](/images/data_feminism_4.jpg)

In the above bar chart, we can see that the title says, “Mental Health in Jail” and the subtitle says, “Rate of mental health diagnosis of inmates.” Based on the visual, one might determine that incarcerated white people have the highest rate of mental health diagnoses. However, what [the research study](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4539829/) was actually showing was that white people are more likely to receive a mental health diagnosis, while Black and Latinx people are more likely to be placed in solitary confinement. The above bar chart does not reflect this context from which the data arises.

![](/images/data_feminism_5.jpg)

The second title gets to the heart of the matter—there is racism in jail, not simply mental health occurrences. This names the forces of oppression at work, the system in which the data we are analyzing exists in.

The imperative of considering context is embodied in the following quote from *Data Feminism*: “It is your responsibility to connect the research question to the results and to the audience’s interpretation of the results. Letting the numbers speak for themselves is emphatically not more ethical or democratic because it often leads to those numbers being misinterpreted or the results of the study being lost.”

![](/images/data_feminism_6.jpg)

Finally, the last iteration of this bar chart changes the subtitle to “White people get more mental health diagnoses.” Now we are highlighting the problem, Racism in Jail, as well as pointing out the system it upholds, which is white supremacy. We are no longer engaging in a victim narrative about people of color, but clearly illuminating the resulting consequence, which is valuing white mental health over the mental health of people of color.

We can see the importance of context in the following video taken by our Indigenous Caravan. We can see a beautiful banner written in both Mayan and Spanish. There are a few curious words and symbols to note. First, it can be seen that the banner says “mineria,” which in the context of the Mayan Train is not relevant. This is actually referring to a different ecological disaster of [mining by the United States and Canada](https://earthjournalism.net/stories/despite-indigenous-resistance-mexico-authorizes-mining-concessions-in-protected-areas) in protected areas—another example of imperialism of the Global North as it colonizes the Global South. There is also a figure with a bullet chain vest and a sombrero, as well as the name “Morelos.” Without an understanding of Mexican history, these symbols are lost on the viewer.

[Video 5 - Protest with Banner of Emiliano Zapata](https://drive.google.com/file/d/1tldWXbx86-jvoxwWqo-Z_woZGznBxk5h/view?usp=sharing)

The figure is Emiliano Zapata, revolutionary hero and founder of the agrarian movement known as *Zapatismo*. Emiliano Zapata was born in Morelos, Mexico. The data cannot be separated from the rich history which gives rise to the symbols.

![](/images/data_feminism_7.jpg)

**The seventh, and final, principle of Data Feminism is to make labor visible.** By making labor visible, we are acknowledging, crediting, and honoring all of the people that go into data science, from the individuals being surveyed, to the people doing the surveying, to the data scientists doing the analyzing, the artists doing the visualization, and more. We also understand that any movement is composed of many different members of the community and from all walks of life. There are artists, musicians, technologists, grandmothers, nurses, and more who contribute to any movement. We exist as a collective and acknowledge the contributions of every member, instead of focusing on those with the most “credentials” or “awards.”

[Video 6 - Protest with Musicians](https://drive.google.com/file/d/17rxENg45fe4ebwhNarsgIVWZwtH4MD_t/view?usp=sharing)
