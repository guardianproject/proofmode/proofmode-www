---
title: 'Don&rsquo;t Get Trumped by AI: Capture Verifiable Breaking News'
date: 2023-04-03 10:00:00
featured_image: '/images/weirdainypd.png'
excerpt: Stop dropping random JPEG photos online and expecting people to trust them, and start using ProofMode and ProofCheck instead
---

Last week, the world was [nearly fooled by AI-generated photos](https://www.bbc.com/news/world-us-canada-65069316) of a former president being arrested, and [pretty much entirely fooled](https://www.theverge.com/2023/3/27/23657927/ai-pope-image-fake-midjourney-computer-generated-aesthetic) by AI-generated photos of a stylish, swagging global religious leader. Now we are on the eve of a potential, actual breaking news moment of the former president, and I am sure a wave of actual newsworthy, authentic photos and videos, along with another round of AI-generated ones.

*The last thing you want as a hard-working journalist, citizen journalists, or citizen witness is to have an generative AI get the credit for your actual documented reality*

### The question is **how will you tell them apart?** This post is a **call to action** for all who plan to try to document world events, whether tomorrow in Manhattan, or anytime around the world.

![image](/images/faketrumpai.jpg)

We, the ["Rebel Geeks" at Guardian Project](https://www.youtube.com/watch?v=ssbezlRkxt8) along with our partners at [Witness.org](https://blog.witness.org/2017/04/proofmode-helping-prove-human-rights-abuses-world/), have been working for nearly a decade on the problem of [capturing and verifying trustworthy photos and videos](https://proofmode.org/blog/three-layers) from your smartphone. We have created a very simple, painfree method of adding digital signatures, notarizations, and extra metadata for verification to any photo or video file you capture on your smartphone device. 

### Don't let some fake AI steal your big moment... Four Easy Steps to Publishing Verifiable Breaking News

1. If you are new to ProofMode, [view our overview presentation here](https://www.beautiful.ai/player/-NHPp4pwCgIqJTMhyeBv/ProofMode-The-Hunt-Baseline-and-Beyond)
2. [Install ProofMode](/install) and use it to capture verifiable photos and videos and share it anywhere you want or need to. You can share proof bundle zip files through Signal, WhatsApp, DropBox, AirDrop, Google Drive... wherever you want, and the verifiability stays intact.
3. Use our [ProofCheck Verification Tool](https://proofcheck.gpfs.link/) to generate and share a verification report of any media you capture.
4. Publish the signatures and links to the original proof zip data alongside any public news article, blog post, or social media post you publish.

#### Here is an example of our [ProofCheck verification tool](https://proofhceck.gpfs.link/) in action, on a [set of photos submitted by one our ProofCorps contributors of a hot air balloon flight](https://ipfs.io/ipfs/bafybeidgaedbfhgubzpvukhftvqfzkoiudxfob4uglrl7xftyx26qt4uc4/)
[![image from an actual real life event](/images/proofcheckballoons.jpg)](https://proofcheck.gpfs.link/#bafybeidgaedbfhgubzpvukhftvqfzkoiudxfob4uglrl7xftyx26qt4uc4/proofmode-7B9EBFF4CBB73FC0-2023-04-02-11-14-14GMT+2.zip)

*You can re-run the verification of this documentation of actual non-AI-generated events [using ProofCheck](https://proofcheck.gpfs.link/#bafybeidgaedbfhgubzpvukhftvqfzkoiudxfob4uglrl7xftyx26qt4uc4/proofmode-7B9EBFF4CBB73FC0-2023-04-02-11-14-14GMT+2.zip)*

*Two more optional steps*
- View our [ProofCorps Contributor Guide](/proofcorps), and learn how to join our effort to verifiably document the planet
- (Optionally) store you media on [Web3.storage](https://web3.storage), a decentralized, resilient storage service backed by the [IPFS protocol](https://ipfs.tech) and [Filecoin](https://filecoin.io/).

### The days of just dropping a random JPEG online and expecting everyone to believe it are over. Set your phones to Proofmode and fight back against the machines.

![weird AI police image](/images/weirdainypd.png)

---
*The "2023-04-02 Stellenbosch Hot Air Balloon Flight" ProofMode proof bundle captured by ProofCorp Contributor Roland Alberston and is licensed under [Creative Commons 3.0 By Attribution](https://creativecommons.org/licenses/by/3.0/us/) license*
