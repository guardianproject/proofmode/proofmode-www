---
title: Capture Reality with ProofMode
description: Get the capture app for Android and iPhone
subtitle: Get the capture app for Android and iPhone
layout: minpage
---

### iPhone, iPad, and Apple Silicon (Mac M1, M2)

* Learn more on the [ProofMode for iOS Project Page](/project/proofmode-ios)
* Now available on the [Apple App Store](https://apps.apple.com/us/app/proofmode/id1526270484)

[![Apple app store](/images/apple.png)](https://apps.apple.com/us/app/proofmode/id1526270484) [![Apple app store](/images/proofmode-qr-apple.png)](https://apps.apple.com/us/app/proofmode/id1526270484)

#### Beta/Nightly iOS test releases

* Step 1: Install [Testflight from Apple](https://itunes.apple.com/us/app/testflight/id899247664?mt=8) to enable beta access
* Step 2: Join the [ProofMode Testflight for iOS Devices](https://testflight.apple.com/join/X2cdrTrK)

### Android

ProofMode for Android is currently available in full, public release via Google Play, and also available as early access beta releases via Github.

* Learn more on the [ProofMode for Android Project Page](/project/proofmode-android)

[![google play store](/images/en-play-badge.png)](https://play.google.com/store/apps/details?id=org.witness.proofmode) [![google play store](/images/proofmode-qr-google-play.png)](https://play.google.com/store/apps/details?id=org.witness.proofmode)

Install from <a href="https://play.google.com/store/apps/details?id=org.witness.proofmode">Google Play</a>

Alternative: [Beta Releases via Github](https://github.com/guardianproject/proofmode-android/releases)

ProofMode is also available through the [Guardian Project F-Droid Repository](https://guardianproject.info/fdroid)



