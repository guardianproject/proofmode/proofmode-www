---
title: Preserving Proof Immutably
description: How to reliably store ProofMode sets and other media on the decentralized web
subtitle: How to reliably store ProofMode sets and other media on the decentralized web
layout: minpage
---

### Future Proof the Truth!

A key aspect of our [Baseline](https://proofmode.org/baseline) project is to demonstrate real-world cases of ["most important information"](https://fil.org) being verifiably captured, notarize, stored and shared using our [ProofMode software](/install) along with other decentralized web services, such as [IPFS](https://ipfs.io) and [Filecoin](https://fil.org). We wanted to have a very public showcase of something other than NFT art that truely made sense to store in a unchangeable or "immutable" manner. We also wanted to publish content that others may be interested in re-using, building upon, or helping preserve themselves, and demonstrating easy steps to do so.

[![baseline image](/images/baseline/caravan2023.png)](/baseline)

As of today, we have preserved nearly 100GB of content on [Baseline](/baseline), ranging from [eyewitness documentation of natural disaster relief](https://proofmode.org/blog/hurricane-otis-proofmode) and [indigenous land rights movements](https://proofmode.org/blog/indigenous-caravan) in Mexico, to a [year in the life in Cape Town](https://baseline.proofmode.org/ipfs/QmPad6dJ4Y9xH62goPCH2KkBkFVXcFKYav6BhV5iWK8N8f/), and background on news reports from the Middle East.

### Make Your Own Baseline

This document will walk you through our process for publishing new content onto the decentralized web, so that you can do it yourself. It is tuned around the specific case of publishing proofmode bundles, which are a specific set of media+metadata files in a zip archive format.

![dweb publishing diagram](/images/preserve.png)

When you complete the steps of this process, your content will be preserved and accessible through multiple redundant points on the decentalized web, and be ready to be easily replicated and made more resilient by any other person, community, or organization who wishes to preserve it. It will also be published in such a way that is resilient to loss, even if one or more components of your system are lost, destroyed, or otherwise compromised.

### Prerequisites

* a desktop or laptop computer running Linux (Ubuntu), Windows, or MacOS (no Chromebooks)
* the [IPFS Desktop application](https://docs.ipfs.tech/install/ipfs-desktop/) 
* An account on [Filebase](https://filebase.com/), an IPFS-capable cloud storage provider (free is fine to start)
* (Optional) A secondary account on [Pinata](https://www.pinata.cloud/), for redundant IPFS cloud pinning
* (Optional) An external hard drive, for redundant physical backup
* (Optional) [thumbsup](https://github.com/thumbsup/thumbsup) open-source photo and video gallery generator

### Steps to Immutability

<hr/>
For the sake of our example, we will be using "Big Bay Surfers" (from our [Cape Town 2023 Baseline Set](https://baseline.proofmode.org/ipfs/QmPad6dJ4Y9xH62goPCH2KkBkFVXcFKYav6BhV5iWK8N8f/2023-02-04-Big-Bay-Surfers.html)) as the example for our proof preservation.

[![big bay surfers](https://baseline.proofmode.org/ipfs/QmPad6dJ4Y9xH62goPCH2KkBkFVXcFKYav6BhV5iWK8N8f/media/large/2023-02-04%20Big%20Bay%20Surfers/proofmode-E4837A017D1B463D-2023-02-04-12-10-45GMT%2B2/IMG_1648.jpg)](https://baseline.proofmode.org/ipfs/QmPad6dJ4Y9xH62goPCH2KkBkFVXcFKYav6BhV5iWK8N8f/2023-02-04-Big-Bay-Surfers.html)

<hr/>

1) Organize your proof bundles on your local disk into a folder location and name that has some meaning, such as "Big Bay Surfers". In this folder, there should be one or more proof bundle zip files, along with any other supporting or related documentation or files that should be preserved. You may optionally unzip each proof bundle into its own separate folder along side each zip. This will make the various contents of the proof bundle available for direct link and download in the future, which can be useful for re-use and verification.

![file folders and zip files](/images/preserve-organize.png)

<hr/>
*Okay, you are ready to roll...*
<hr/>

2) Open the IPFS Desktop app, and import the entire "Big Bay Surfers" folder. At this point, it will create an [IPFS Content ID](https://docs.ipfs.tech/concepts/content-addressing/) for each folder and file you are adding. A content identifier, or CID, is a label used to point to material in IPFS. It doesn't indicate where the content is stored, but it forms a kind of address based on the content itself. You can see a related walkthrough on [publishing a website to IPFS here](https://docs.ipfs.tech/how-to/websites-on-ipfs/single-page-website/#add-your-site).

![ipfs desktop app](/images/preserve-ipfs-import2.png)

![ipfs desktop app](/images/preserve-ipfs-import1.png)

<hr/>
*Congratulations, your proof bundle content is now officially published on the InterPlanetary File Service! Now to make sure that there are many copies...*
<hr/>

3) Setup IPFS Desktop app with one or more [IPFS pinning services](https://docs.ipfs.tech/concepts/persistence/#persistence-versus-permanence), such as the [Filebase](https://filebase.com) or [Pinata](https://pinata.cloud) services we recommend. [Follow the steps here to do so](https://docs.ipfs.tech/how-to/work-with-pinning-services/) 

![ipfs desktop app](/images/preserve-add-pinning.png)

<hr/>
*Pinning is how you ensure your local node content spreads far and wide, and remains there!*
<hr/>

4) Using the IPFS Desktop user interface, click on the box next to the "Big Bay Surfers" folder, and pin it on the configured services. This will request that each pinning service makes a cached copy of your content, through the IPFS Content ID and protocol. It ensures that the "Big Bay Surfers" folder and content within, is not just accessible on your very low priority and profile desktop IPFS node, but also reachable through the much more highly available Filebase and Pinata IPFS nodes, and all of their peered nodes. 

![ipfs desktop app](/images/preserve-ipfs-pinning3.png)

![ipfs desktop app](/images/preserve-ipfs-pinning4.png)

<hr/>
*Now your proof bundles are not just accessible via your limited IPFS node, but replicated to very reachable, highly available cloud nodes*
<hr/>

5) Now, also pin each file within the "Big Bay Surfers" folder, using the same process from step 4 above. This will ensure that each individual proof bundle can be reached directly via its own IPFS Content ID, and not just as a subpath under the parent folder's content ID.

<hr/>
*Pinning each proof zip makes it more efficent and less data intesive, to share and access just one piece of specific proof*
<hr/>

6) You can now test if your content is available, using the Content IDs directly. This can be done using the [IPFS Desktop "Download" feature](https://docs.ipfs.tech/how-to/desktop-app/#download) or [IPFS Kubo command line interface](https://docs.ipfs.tech/how-to/kubo-basic-cli/#install-kubo) directly, via the following command

	> ipfs get <contentid>

![ipfs get command](/images/preserve-ipfs-get.png)

<hr/>
*It may make sense to try this step on another computer than the one you are using, so you can be sure the content is reachable remotely*
<hr/>


7) You can now also test using an IPFS HTTP Gateway, such as the one available at https://ipfs.io/ipfs or through the [IPFS gateway provided by Filecoin](https://docs.filebase.com/ipfs/about-ipfs/ipfs-gateways) or [Pinata](https://www.pinata.cloud/dedicated-gateways). 

You can access some of our Baseline content at [https://ipfs.io/ipfs/QmZNTD4tAe8ELX1VosM8buQQcCg4S9zG5N2Xp2ofqCfSrF](https://ipfs.io/ipfs/QmZNTD4tAe8ELX1VosM8buQQcCg4S9zG5N2Xp2ofqCfSrF)

![ipfs gateway](/images/preserve-ipfs-gateway.png)

<hr/>
*While we all wish that all browsers spoke the IPFS protocol directly, only Brave browser does at this time, so HTTP gateways are still needed.*
<hr/>

8) Ask someone else to pin your content from their own IPFS or Filebase account using the "Big Bay Surfers" folder's or individual proof zip's IPFS Content ID. This ensures a verifiable bit-for-bit copy of the content now exists cached in their node.

	> ipfs pin add <contentid>

![ipfs pin](/images/preserve-ipfs-pin-cli.png)

![ipfs pin](/images/preserve-ipfs-pin-filebase.png)

<hr/>
*The more people that pin your content, the more places it is preserved and cached, which speeds up access, and decreases the likelihood it will ever be lost*
<hr/>

9) [Export the "Big Bay Surfers" Content Archive (CAR) file](https://web3.storage/docs/concepts/car/) from local IPFS node using the [Kubo command line tool](https://web3.storage/docs/concepts/car/#kubo) and store on the secondary external hard drive. 

	> ipfs dag export <contentid> > yourfilename.car

as shown here:

![ipfs dag export command](/images/preserve-ipfs-dag-export.png)

Once this is completed, the external drive should be stored in a separate, secured physical location, such as another office or safety deposit box.

![external drive](/images/preserve-drive.jpg)

<hr/>
*This is the worst case scenario insurance policy that will allow you to restore access to your content onto the IPFS network, in its original, immutable form and identical Content ID.*
<hr/>

10) [Upload your CAR files](https://web3.storage/docs/how-to/upload/) to the Web3.storage service to make them available on the Filecoin storage network.

![ipfs web3.storage car uplaod](/images/preserve-web3-car1.png)

![ipfs web3.storage car uplaod](/images/preserve-web3-car2.png)

![ipfs web3.storage car uplaod](/images/preserve-web3-car3.png)

<hr/>
*Yes, [IPFS and Filecoin are different](https://docs.filecoin.io/basics/how-storage-works/filecoin-and-ipfs), and it is useful to have the identical content natively published on both*
<hr/>

### There Are Many Copies, In Many Places

If you have completed all of these steps, your "Big Bay Surfers" proof bundles are now available on your local machine, through multiple nodes and cache points on the IPFS network, pinned on your friends' IPFS nodes and accounts, backed up to an external physically secured hard drive, and stored by commercial provides on the Filecoin network, all through the same, immutable, unchangeable content identifier. Huzzah! 

[![many me](/images/me_sliding.gif)](https://erdalinci.tumblr.com/)
_Animation by [Erdal Inci](https://erdalinci.tumblr.com/)_

Please visit our [ProofMode Baseline](https://proofmode.org/baseline) directory to browse some of the verifiable, immutable photo and video documentation of actual reality that we have published using this process. If you have questions or need assistance, [do not hesitate to contact us](/contact).
