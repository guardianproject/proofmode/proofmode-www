---
layout: minpage
---

<center>
<p>ProofMode captures, authenticates and verifies smartphone multimedia from source to recipient. It enhances metadata, fingerprints hardware, cryptographically signs content, and uses third-party notaries for a decentralized, privacy-focused chain of custody—empowering activists, journalists, and everyday people.</p>
<p><b>In short, it helps people know your photos and videos are really real.</b>
<div><a href="/install/"><img src="/images/proof-feature.png"/></a></div>
<div> <a href="https://apps.apple.com/us/app/proofmode/id1526270484"><img src="/images/apple.png"></a><a href="https://play.google.com/store/apps/details?id=org.witness.proofmode"><img src="/images/en-play-badge.png"></a> </div>
</center>

<h3>"We believe in a future, where every camera will have a 'Proof Mode' that can be enabled and every viewer an ability to verify-then-trust what they are seeing."<br/>- <a href="/blog/witness-techadvo">Sam Gregory, Witness.org 2013</a></h3>

<p>ProofMode supports the <a href="/c2pa">Coalition for Content Provenance and Authentication (C2PA) standard</a>, <a href="/c2pa">Content Credentials</a> and the <a href="/c2pa">Content Authenticity Initiative</a>.</p>

<h4>How can I use ProofMode today?</h4>
<p>
ProofMode is ready to use and publicly available as <a href="/install/">production mobile capture apps</a>, <a href="/verify">verification tools</a>, <a href="/project/proofmode-for-devs">open-source developer libraries</a>. We also provide training and support for the use of resilient decentralized storage technology through our <a href="/preserve/">PRESERVE Process</a>.
</p>


