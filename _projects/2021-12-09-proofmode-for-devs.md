---
title: 'ProofMode for Developers'
subtitle: 'Code and libraries for developers to integrate ProofMode'
date: 2021-12-10 00:00:00
featured_image: '/images/mobiledev.png'
excerpt: Learn how you can add ProofMode capability to your apps and services.
---

## LibProofMode

ProofMode developer libraries for Android and iOS are available for integration into any mobile application.

* libProofMode for Android [Gitlab Code and Sample](https://gitlab.com/guardianproject/proofmode/libproofmode-android-sample)
* libProofMode for iOS [Gitlab Code and Sample](https://gitlab.com/guardianproject/proofmode/libproofmode-ios)
* Blog post covering [sample integration with a camera app](/blog/integrating-proofmode-library)

## SimpleC2PA

This project builds upon the [C2PA Rust library](https://github.com/contentauth/c2pa-rs) to provide an easy solution for mobile apps to add signed C2PA actions, claims, and attestations to media files. It also includes support for generating a private key and self-signed x509 certificate entirely locally on the device.

* SimpleC2PA Mobile SDK [Gitlab Code and Sample](https://gitlab.com/guardianproject/proofmode/simple-c2pa)
