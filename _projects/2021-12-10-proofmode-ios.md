---
title: 'ProofMode for iOS'
subtitle: 'Free and open-source app for iPhone, iPad and MacOS'
date: 2021-12-10 00:00:00
featured_image: '/images/appstore.png'
excerpt: Learn more about what we are up to on the iOS platform.
---

ProofMode for iOS is available on the [Apple App Store](https://apps.apple.com/us/app/proofmode/id1526270484) for iPhone, iPad, and any Mac M1 or M2 computer.

* [Testflight for Beta/Nightly Builds](https://testflight.apple.com/join/X2cdrTrK) available.
* [Gitlab Project](https://gitlab.com/guardianproject/proofmode-ios/-/tree/main_npex).

<hr/>
[<img src="/images/proofmodeappstore.jpg">](https://apps.apple.com/us/app/proofmode/id1526270484)
